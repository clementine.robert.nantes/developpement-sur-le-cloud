# Projet Développement sur le Cloud

Dans le cadre du module Développement d'applications sur le Cloud, encadré par P. MOLLI nous avons eu a réaliser un projet avec Google AppEngine et le Datastore afin de créer une application web de signature de pétitions en ligne.  
  
Ce projet a été réalisé par Emma BRETAUD, Ludwig GRANDIN & Clémentine ROBERT.      

M1 MIAGE Classique.    

# Liens relatifs à l'application
- [Application](https://orbital-nova-291715.appspot.com/). 
- [Accès aux API](https://apis-explorer.appspot.com/apis-explorer/?base=https%3A%2F%2Forbital-nova-291715.appspot.com%2F_ah%2Fapi&root=https%3A%2F%2Forbital-nova-291715.appspot.com%2F_ah%2Fapi#p/myApi/v1/). 
- [GitLab](https://gitlab.com/clementine.robert.nantes/developpement-sur-le-cloud). 

# Fonctionnalités à implémenter
- Se connecter avec son compte Google ✅
- Ajouter une pétition ✅
- Signer une pétition, sans pouvoir la signer 2 fois ✅
- Rechercher une pétition ✅ (Par titre ou par tags)
- Afficher les pétitions signées d'un utilisateur triées par date ✅
- Afficher le top 10 des pétitions triées par date ✅

# Ce qui ne fonctionne pas
- La suppression fonctionne mais pour le voir il faut réactualiser la page. 
- Quand on vote trop rapidement sur 2 pétitions différentes, le vote se décale sur la pétition précédente.  
- L'ajout d'une pétition fonctionne mais nous n'avons pas réussi à ajouter un tableau de Tags. La recherce pour les pétitions récemment ajoutées se fera donc que sur le titre.
- Attention la recherche est sensible à la casse. 
- Il n'y pas de déconnexion, l'utilisateur est déconnecté à chaque rafraichissement de la page. 

# Ce qui pourrait être amélioré
- Une application un peu plus jolie. 
- Ajouter les pétitions avec les tags. 
- Modifier une pétition. 
- Une fois qu'on a voté pour une pétition qu'il soit écrit "Retiré mon vote". 

# Entités
# User
![](images_readme/user_java.png)

![](images_readme/user_datastore.png)

# Pétion
![](images_readme/petition_java.png)

![](images_readme/petition_datastore.png)

# Captures d'écran de l'application
![](images_readme/accueil.png)
![](images_readme/top_petition.png)
![](images_readme/my_pet_signed.png)
![](images_readme/add_petition.png)
![](images_readme/my_pet_created.png)




