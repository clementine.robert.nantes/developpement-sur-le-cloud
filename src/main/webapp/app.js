m.route(document.body, "/", {
    "/" : {
        onmatch: function() {
            return MyApp.Top10;
        }
    },
    "/myPetSigned" : {
        onmatch: function() {
            return MyApp.MyPetSigned;
        }
    },

    "/newPetition" : {
        onmatch: function() {
            return MyApp.AddPetition;
        }
    },

    "/top10" : {
        onmatch: function() {
            return MyApp.Top10;
        }
    },

    "/myPetCreated" : {
        onmatch: function() {
            return MyApp.MyPetCreated;
        }
    },

    "/home" : {
        onmatch: function() {
            if (!auth2.isSignedIn.get()) return MyApp.NotSignedIn;
            else return MyApp.MyApp.Top10;
        }
    },

    
    "/search": {
        onmatch : function () {
            return MyApp.PetitionSearch;
        }
    },
    
    "/login": {
        onmatch: function () {
            return MyApp.Login;
        }
    },
    
});

var showProfile = false;
var showSearchList = false;
var showTop10 = false;

var isLoggedIn = false;
var auth2;
var googleUser;


gapi.load('auth2', function() {
    auth2 = gapi.auth2.init({
        client_id: "593310797272-uo5kpvrgtk88p33h8jrd6bdeq1714h2v.apps.googleusercontent.com"
    });
    auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);

    auth2.isSignedIn.listen(signinChanged);
    auth2.currentUser.listen(userChanged); 
});

var signinChanged = function (loggedIn) {

    if (auth2.isSignedIn.get()) {
        googleUser = auth2.currentUser.get();
        isLoggedIn = loggedIn;
        if(loggedIn) {
            MyApp.Profile.userData.name = googleUser.getBasicProfile().getName();
            MyApp.Profile.userData.email = googleUser.getBasicProfile().getEmail();
            MyApp.Profile.userData.firstName = googleUser.getBasicProfile().getGivenName();
            MyApp.Profile.userData.lastName= googleUser.getBasicProfile().getFamilyName();
            MyApp.Profile.userData.id = googleUser.getAuthResponse().id_token;
            MyApp.Profile.userData.url = googleUser.getBasicProfile().getImageUrl();

        } else {
            MyApp.Profile.userData.name = "";
            MyApp.Profile.userData.firstName = "";
            MyApp.Profile.userData.lastName = "";
            MyApp.Profile.userData.email = "";
            MyApp.Profile.userData.id = "";
            MyApp.Profile.userData.url = "";
        }
    }
};

var onSuccess = function(user) {
    googleUser = user;
    var profile = user.getBasicProfile();

    MyApp.Profile.userData.name = profile.getName();
    MyApp.Profile.userData.firstName = profile.getGivenName();
    MyApp.Profile.userData.lastName= profile.getFamilyName();
    MyApp.Profile.userData.email = profile.getEmail();
    MyApp.Profile.userData.id = user.getAuthResponse().id_token;
    MyApp.Profile.userData.url = profile.getImageUrl();

    isLoggedIn=true;

    MyApp.Profile.createUser();
    MyApp.Top10.getTop10();
    MyApp.MyPetSigned.getMyPetitionSigned();
    MyApp.MyPetCreated.getMyPetCreated();

    m.route.set("/home");
};

var onFailure = function(error) {
    console.log(error);
};


var userChanged = function (user) {
    if(user.getId()){
      // Do something here
    }
};

var MyApp = {
    view: function (ctrl) {
        return (
            m("div", [
                m(MyApp.Navbar, {}),

            ])
        );
    }
};

MyApp.Navbar = {
    view: function () {
        return m("nav", {class: "navbar is-dark"}, [
            m("div", {class: "navbar-brand"}, [
                m(m.route.Link, {href: "/", class: "navbar-item"}, [
                    m("p","TinyPet")
                ])
            ]),
            m("div", {class:"navbar-menu"}, [
                m("div", {class:"navbar-start"}, [
                    
                ]),
                m("div", {class:"navbar-end"}, [
                    m("div", {class:"navbar-item"}, [
                        m(MyApp.Searchbar),
                        m("p", MyApp.Profile.userData.name)
                    ]),
    
                    m("div", {class: "navbar-item"}, [
                        m("figure", {class: "image is-32x32"}, [
                            m("img", {
                                class: "is-rounded",
                                "src": MyApp.Profile.userData.url
                            })
                        ])
                    ]),
                    
                    m("div", {class:"navbar-item"}, [
                        m("div", {class:"buttons"}, [
                            m("span", {class: "g-signin2", id:"signin-button"}, [
                                m("p","Se connecter")
                            ])
                        ])
                    ])
                ])
            ]),
        ])
    },

}

MyApp.signInButton = {
    view: function () {
        return m("div", {
                "class":"g-signin2",
                "id":"signin-button"
            }
        );
    }
};

MyApp.MyPetSigned = {
    petitionsSigned: [],
    loading_gif: false,
    view: function () {
        if (MyApp.Profile.userData.id == "") return m(MyApp.NotSignedIn);
        else {
            return m("div", [
                m(MyApp.Navbar),
                m("div.container", [
                    m("h1.title","Mes pétitions signées"),
                    m("button", { class: "button is-primary buttonSpace",
                    onclick: function (){
                        m.route.set("/newPetition")
                    }}, "Ajouter une nouvelle pétition"),
                    m("button", {class: "button is-primary buttonSpace",
                        onclick: function () {
                            m.route.set("/myPetCreated");
                        }
                    }, "Mes pétitions créées"),
                        MyApp.Top10.loading_gif?
                            m("div",
                                m("img", {
                                    "style":"text-center",
                                    "src":"img/loading.gif",
                                    "alt":"Loading..."
                                })
                            )
                            :
                            MyApp.MyPetSigned.petitionsSigned.length==0?
                                m("div",
                                    m("span", "Pas de nouvelles pétitions à afficher")
                                ):
                                m('table', {
                                    class:'table is-striped',"table":"is-striped"
                                },[
                                    m('tr', [
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Name"),
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Créateur"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Descritpion"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Tags"),
                                        m('th', {
                                            "style":"width:25vw"
                                        }, "Nombre de votants"),
                                        m('th', {
                                            "style":"width:10vw"
                                        }),
                                    ]),
                                    MyApp.MyPetSigned.petitionsSigned.map(function(petition) {
                                        return m("tr", [
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.name)
                                            ),
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.owner)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.body)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.tags)
                                            ),
                                            m('td', {
                                                "style":"width:25vw"
                                            }, m('label', petition.nbVotants)
                                            ),

                                            m("td", {
                                                "style":"width:10vw"
                                            },
                                                    m("button", {
                                                        "class":"button is-light",
                                                        onclick: function () {
                                                            MyApp.MyPetSigned.votePetition(petition.key.name);
                                                        },
                                                },
                                                "Voter")
                                            )
                                        ]);
                                    })
                                ]),
                                m('button',{
                                    class: 'button is-info',
                                    onclick: function(e) {
                                        MyApp.MyPetSigned.getNextPetitions();
                                    }
                                }, "Petitions suivantes"),

                ])
            ]);
        }

    },
    getMyPetitionSigned : function(){
        MyApp.Top10.loading_gif = true;
        
        m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/myPetSigned",
            params: {
                "user" : MyApp.Profile.userData.email,
                "email" : MyApp.Profile.userData.email,
                "access_token" : encodeURIComponent(MyApp.Profile.userData.id)
            }
        })
        .then(function(response) {
            showTop10 = true;
            MyApp.Top10.loading_gif = false;
            var i = 0;
            if (response.items === undefined) {
                MyApp.MyPetSigned.petitionsSigned = [];
            } else {
                response.items.forEach( function (petition) {
                    MyApp.MyPetSigned.petitionsSigned[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        "tags": petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                    console.log(MyApp.MyPetSigned.petitionsSigned);
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.userData.nextToken="";
                }
            }
        });
        m.route.set("/myPetSigned")
    },

    votePetition: function (votePet) {
	    var data = {
            'petitionChoosed': votePet,
            'email': MyApp.Profile.userData.email,
            'access_token' : encodeURIComponent(MyApp.Profile.userData.id)
        };
        m.request ({
	 		method: "POST",
            url: "_ah/api/myApi/v1/petition/newVote",
            params: data,
		}).then(function(response){
            MyApp.MyPetSigned.getMyPetitionSigned();
            m.redraw();
        });
    },

    getNextPetitions: function() {
        return m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/myPetSigned",
            params: {
                'email': MyApp.Profile.userData.email,
                'next':MyApp.Profile.userData.nextToken,
                'access_token': encodeURIComponent(MyApp.Profile.userData.id)
            }
        })
        .then(function(response) {
            showTop10 = true;
            MyApp.Top10.loading_gif = false;
            var i = 0;
            if (response.items != undefined) {
                response.items.forEach( function (petition) {
                    MyApp.MyPetSigned.petitionsSigned[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        "tags" : petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.nextToken="";
                }
            }
        });
    },
}

MyApp.Top10 = {
    petitions: [],
    loading_gif: false,
    view: function () {
        if (MyApp.Profile.userData.id == "") return m(MyApp.NotSignedIn);
        else {
            return m("div", [
                m(MyApp.Navbar),
                m("div.container", [
                    m("h1.title","Top pétitions"),
                    m("button", { class: "button is-primary buttonSpace",
                    onclick: function (){
                        m.route.set("/newPetition")
                    }}, "Ajouter une nouvelle pétition"),
                    m("button", { class: "button is-primary buttonSpace",
                        onclick: function () {
                            MyApp.MyPetSigned.getMyPetitionSigned();
                            m.route.set("/myPetSigned")
                            
                        }
                    }, "Afficher mes pétitions signées"),
                    m("button", {class: "button is-primary buttonSpace",
                        onclick: function () {
                            m.route.set("/myPetCreated");
                        }
                    }, "Mes pétitions créées"),
                        MyApp.Top10.loading_gif?
                            m("div",
                                m("img", {
                                    "style":"text-center",
                                    "src":"img/loading.gif",
                                    "alt":"Loading..."
                                })
                            )
                            :
                            MyApp.Top10.petitions.length==0?
                                m("div",
                                    m("span", "Pas de nouvelles pétitions à afficher")
                                ):
                                m('table', {
                                    class:'table is-striped',"table":"is-striped"
                                },[
                                    m('tr', [
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Name"),
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Créateur"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Descritpion"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Tags"),
                                        m('th', {
                                            "style":"width:25vw"
                                        }, "Nombre de votants"),
                                        m('th', {
                                            "style":"width:10vw"
                                        }),
                                    ]),
                                    MyApp.Top10.petitions.map(function(petition) {
                                        return m("tr", [
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.name)
                                            ),
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.owner)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.body)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.tags)
                                            ),
                                            m('td', {
                                                "style":"width:25vw"
                                            }, m('label', petition.nbVotants)
                                            ),

                                            m("td", {
                                                "style":"width:10vw"
                                            },
                                                    m("button", {
                                                        "class":"button is-light",
                                                        onclick: function () {
                                                            MyApp.Top10.votePetition(petition.key.name);
                                                        },
                                                },
                                                "Voter")
                                            )
                                        ]);
                                    })
                                ]),
                                m('button',{
                                    class: 'button is-info',
                                    onclick: function(e) {
                                        MyApp.Top10.getNextPetitions();
                                    }
                                }, "Pétitions suivantes"),

                ])
            ]);
        }
    },
    getTop10 : function () {
        MyApp.Top10.loading_gif = true;
        m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/topPet",
            params : {
                "access_token" : encodeURIComponent(MyApp.Profile.userData.id)
            }
        })
        .then(function(response) {
            showTop10 = true;
            MyApp.Top10.loading_gif = false;
            var i = 0;
            if (response.items === undefined) {
                MyApp.Top10.petitions = [];
            } else {
                response.items.forEach( function (petition) {
                    MyApp.Top10.petitions[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        "tags": petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.userData.nextToken="";
                }
            }
        });
    },
    getNextPetitions: function() {
        return m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/topPet",
            params: {
                'email': MyApp.Profile.userData.email,
                'next':MyApp.Profile.userData.nextToken,
                'access_token': encodeURIComponent(MyApp.Profile.userData.id)
            }
        })
        .then(function(response) {
            showTop10 = true;
            MyApp.Top10.loading_gif = false;
            var i = 0;
            if (response.items != undefined) {
                response.items.forEach( function (petition) {
                    MyApp.Top10.petitions[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        "tags":petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.nextToken="";
                }
            }
        });
    },
    


    votePetition: function (votePet) {
	    var data = {
            'petitionChoosed': votePet,
            'email': MyApp.Profile.userData.email,
            "access_token" : encodeURIComponent(MyApp.Profile.userData.id)
        };
        m.request ({
	 		method: "POST",
            url: "_ah/api/myApi/v1/petition/newVote",
            params: data,
		}).then(function(response){
            MyApp.Top10.getTop10();
            m.redraw();
        });
    },
};

MyApp.NotSignedIn = {
    view: function () {
        return m("div", [
            m(MyApp.Navbar),
            m("div.container", [
                m("div.row.mb-3",
                    m("div", {
                        class:"title col-md-12 col-sm-12 col-xs-12"
                    },
                        m("h1", "Connectez-vous pour accéder à nos pétitions"))
                ),
                m("img", {
                    "style":"text-center",
                    "src":"img/welcome.gif",
                    "alt":"Welcome"
                })
                
            ])
        ]);
    }
};

MyApp.Profile = {
    userData: {
        name: "",
        firstName: "",
        lastName: "",
        email: "",
        id: "",
        url: "",
        content:"",
        nextToken:"",
    },
    
    createUser: function() {
        var data = {
            'email': MyApp.Profile.userData.email,
            'firstName': MyApp.Profile.userData.firstName,
            'lastName': MyApp.Profile.userData.lastName,
            'name': MyApp.Profile.userData.name,
            'invertedName': MyApp.Profile.userData.lastName + " " + MyApp.Profile.userData.firstName,
            'url': MyApp.Profile.userData.url,
            'access_token': encodeURIComponent(MyApp.Profile.userData.id)
        };
        m.request ({
            method: "POST",
            url: "_ah/api/myApi/v1/user/createUser",
            params: data,
        }).then(function (response){
        });
    },
};


MyApp.Login = {
    view: function() {
        return m('div',[
            m(MyApp.Navbar),
            m('div.container',[
                m("h1.title", 'Connecte toi avec ton compte Google pour accéder à nos pétitions.'),
                m("h2", "Si le bouton n'apparait pas, rafraichis la page."),
            ])
        ]);
    }
};



MyApp.AddPetition = {
view: function(){
    return m('div',[
        m(MyApp.Navbar),
        m('div', {class:'container mt-5'},[
            m(".collapse[id='collapseNewPost'].mb-5", [
                m("form", {
                    onsubmit: function(e) {
                        e.preventDefault();
                        var name = "";
                        var body = "";
                        var tags = "";

                        if ($("#name").val()=="") name="Petition Name"+Date.now();
                        else name = $("#name").val();
                        if ($("#body").val()=="") post_body="Description"+Date.now();
                        else body = $("#body").val();
                        if ($("#tags").val()=="") post_body="Tags"+Date.now();
                        else tags = $("#tags").val();
                        MyApp.AddPetition.newPetition(name,body,tags);
                    }},
                    [
                        m('div', {
                            class:'field'
                        },[
                            m("label", {
                                class:'label',
                            },"Titre"),
                            m('div',{class:'control'},
                                m("input[type=text]", {
                                    class:'input is-rounded',
                                    placeholder:"Titre de la pétition",
                                    id:"name"
                                })
                            ),
                        ]),
                        m('div',{class:'field'},[
                            m("label", {class: 'label'},"Description"),
                            m('div',{class:'control'},
                                m("input[type=textarea]", {
                                    class:'textarea',
                                    placeholder:"Descritpion de la pétition",
                                    id:"body"
                                })
                            ),
                        ]),

                        m('div',{class:'field'},[
                            m("label", {class: 'label'},"Tags"),
                            m('div',{class:'control'},
                                m("input[type=textarea]", {
                                    class:'textarea',
                                    placeholder:"#Tag1, #Tag2, #Tag3",
                                    id:"tags"
                                })
                            ),
                        ]),

                        m('div',{class:'control mt-3'},
                            m("button[type=submit]", {
                                class:'button is-success'
                            },"Publier")
                        ),
                    ]
                ),
                
            ]),
        ])
    ]);
},
newPetition: function(name,body,tags) {
    var arrayTag = new Array();
    data= {
        'name': name,
        'body': body,
        'tags' : arrayTag, //Ca devrait être tags
        'access_token': encodeURIComponent(MyApp.Profile.userData.id)
    }
    return m.request({
        method: "POST",
        url: "_ah/api/myApi/v1/petition/postPet",
        params: data,
    })
    .then(function(response) {
        MyApp.Top10.getTop10();
        m.route.set("/");
    });
}, 
}


MyApp.Searchbar = {
    view: function () {
        if(MyApp.Profile.userData.id!="") {
            return m("div", {class:"navbar-item"}, [
                    m("form[action='/search'][method='post']", [
                        m("div", {class:"field has-addons"}, [
                            m("p", {class:"control"}, [
                                m("input.input[aria-label='Rechercher'][id='petitionSearch'][name='rechercher'][placeholder='Rechercher une pétition'][type='search']")
                            ]),
                            m("input[id='myEmail'][name='myEmail'][type='hidden'][value=" + MyApp.Profile.userData.email + "]"),
                            m("p", {class:"control"}, [
                                m("button.button.is-primary[type='submit']",{
                                    onclick: function (e) {
                                        e.preventDefault();
                                        MyApp.Searchbar.searchPetition();
                                    }
                                } , "Rechercher"),
                            ])
                    ])
                ])
            ])
        }
    },
    searchPetition: function () {
        m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/search",
            params: {
                'email' : MyApp.Profile.userData.email,
                'recherche':$("#petitionSearch").val(),
                'access_token': encodeURIComponent(MyApp.Profile.userData.id)
            },
        })
        .then(function(response) {
            var i = 0;
            var petition = {};
            if(response.items === undefined) {
                MyApp.PetitionSearch.listePetitions = [];
            } else {
                response.items.forEach(function (item) {
                    petition=item.properties;
                    petKey=item.key;
                    MyApp.PetitionSearch.listePetitions[i] = {
                        key:petKey,
                        name:petition.name,
                        owner:petition.owner,
                        body:petition.body,
                        tags:petition.tags.join(),
                        nbVotants:petition.nbVotants
                    };
                    i++;
                });
            }
            m.route.set("/search");
        });
    }

}

MyApp.PetitionSearch = {
    listePetitions: [],
    loading_gif: false,
    view: function () {
        if (MyApp.Profile.userData.id == "") return m(MyApp.NotSignedIn);
        else {
            return m("div", [
                m(MyApp.Navbar),
                m("div.container", [
                    m("h1.title","Résultats"),
                    m("button", { class: "button is-primary buttonSpace",
                    onclick: function (){
                        m.route.set("/newPetition")
                    }}, "Ajouter une nouvelle pétition"),
                    m("button", { class: "button is-primary buttonSpace",
                        onclick: function () {
                            MyApp.MyPetSigned.getMyPetitionSigned();
                            m.route.set("/myPetSigned")
                            
                        }
                    }, "Afficher mes pétitions signées"),
                    m("button", {class: "button is-primary buttonSpace",
                        onclick: function () {
                            m.route.set("/myPetCreated");
                        }
                    }, "Mes pétitions créées"),
                        MyApp.Top10.loading_gif?
                            m("div",
                                m("img", {
                                    "style":"text-center",
                                    "src":"img/loading.gif",
                                    "alt":"Loading..."
                                })
                            )
                            :
                            MyApp.PetitionSearch.listePetitions.length==0?
                                m("div",
                                    m("span", "Aucun résultat")
                                ):
                                m('table', {
                                    class:'table is-striped',"table":"is-striped"
                                },[
                                    m('tr', [
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Name"),
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Créateur"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Descritpion"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Tags"),
                                        m('th', {
                                            "style":"width:25vw"
                                        }, "Nombre de votants"),
                                        m('th', {
                                            "style":"width:10vw"
                                        }),
                                    ]),
                                    MyApp.PetitionSearch.listePetitions.map(function(petition) {
                                        return m("tr", [
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.name)
                                            ),
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.owner)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.body)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.tags)
                                            ),
                                            m('td', {
                                                "style":"width:25vw"
                                            }, m('label', petition.nbVotants)
                                            ),

                                            m("td", {
                                                "style":"width:10vw"
                                            },
                                                    m("button", {
                                                        "class":"button is-light",
                                                        onclick: function () {
                                                            MyApp.PetitionSearch.votePetition(petition.key.name);
                                                        },
                                                },
                                                "Voter")
                                            )
                                        ]);
                                    })
                                ]),
                                m('button',{
                                    class: 'button is-info',
                                    onclick: function(e) {
                                        MyApp.Top10.getNextPosts();
                                    }
                                }, "Next"),

                ])
            ]);
        }
    },
    votePetition: function (votePet) {
	    var data = {
            'petitionChoosed': votePet,
            'email': MyApp.Profile.userData.email
        };
        m.request ({
	 		method: "POST",
            url: "_ah/api/myApi/v1/newVote"+'?access_token='+encodeURIComponent(MyApp.Profile.userData.id),
            params: data,
		}).then(function(response){
            MyApp.Searchbar.searchPetition();
            m.redraw();
        });
    }
}

MyApp.MyPetCreated = {

    petitions: [],
    loading_gif: false,
    view: function () {
        if (MyApp.Profile.userData.id == "") return m(MyApp.NotSignedIn);
        else {
            return m("div", [
                m(MyApp.Navbar),
                m("div.container", [
                    m("h1.title","Mes pétitions créées"),
                    m("button", { class: "button is-primary buttonSpace",
                    onclick: function (){
                        m.route.set("/newPetition")
                    }}, "Ajouter une nouvelle pétition"),
                    m("button", { class: "button is-primary buttonSpace",
                        onclick: function () {
                            MyApp.MyPetSigned.getMyPetitionSigned();
                            m.route.set("/myPetSigned")
                            
                        }
                    }, "Afficher mes pétitions signées"),
                        MyApp.MyPetCreated.loading_gif?
                            m("div",
                                m("img", {
                                    "style":"text-center",
                                    "src":"img/loading.gif",
                                    "alt":"Loading..."
                                })
                            )
                            :
                            MyApp.MyPetCreated.petitions.length==0?
                                m("div",
                                    m("span", "Vous n'avez créé aucune pétition")
                                ):
                                m('table', {
                                    class:'table is-striped',"table":"is-striped"
                                },[
                                    m('tr', [
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Name"),
                                        m('th', {
                                            "style":"width:20vw"
                                        }, "Créateur"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Descritpion"),
                                        m('th', {
                                            "style":"width:30vw"
                                        }, "Tags"),
                                        m('th', {
                                            "style":"width:25vw"
                                        }, "Nombre de votants"),
                                        m('th', {
                                            "style":"width:10vw"
                                        }),
                                    ]),
                                    MyApp.MyPetCreated.petitions.map(function(petition) {
                                        return m("tr", [
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.name)
                                            ),
                                            m('td', {
                                                "style":"width:20vw"
                                            }, m('label', petition.owner)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.body)
                                            ),
                                            m('td', {
                                                "style":"width:30vw"
                                            }, m('label', petition.tags)
                                            ),
                                            m('td', {
                                                "style":"width:25vw"
                                            }, m('label', petition.nbVotants)
                                            ),

                                            m("td", {
                                                "style":"width:10vw"
                                            },
                                                    m("button", {
                                                        "class":"button is-danger",
                                                        onclick: function () {
                                                            MyApp.MyPetCreated.deletePetition(petition.key.name);
                                                        },
                                                },
                                                "Supprimer")
                                            )
                                        ]);
                                    })
                                ]),
                                m('button',{
                                    class: 'button is-info',
                                    onclick: function(e) {
                                        MyApp.MyPetCreated.getNextPetitions();
                                    }
                                }, "Petitions suivantes"),

                ])
            ]);
        }
    },
    getMyPetCreated : function () {
        MyApp.MyPetCreated.loading_gif = true;
        m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/myPetCreated",
            params: {
                'email': MyApp.Profile.userData.email,
                'access_token' : encodeURIComponent(MyApp.Profile.userData.id) 
            }
        })
        .then(function(response) {
            showMyPetCreated = true;
            MyApp.MyPetCreated.loading_gif = false;
            var i = 0;
            if (response.items === undefined) {
                MyApp.MyPetCreated.petitions = [];
            } else {
                response.items.forEach( function (petition) {
                    MyApp.MyPetCreated.petitions[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        //"tags":petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.userData.nextToken="";
                }
            }
        });
    },
    getNextPetitions: function() {
        return m.request({
            method: "GET",
            url: "_ah/api/myApi/v1/petition/myPetCreated",
            params: {
                'email': MyApp.Profile.userData.email,
                'next':MyApp.Profile.userData.nextToken,
                'access_token': encodeURIComponent(MyApp.Profile.userData.id)
            }
        })
        .then(function(response) {
            showTop10 = true;
            MyApp.Top10.loading_gif = false;
            var i = 0;
            if (response.items != undefined) {
                response.items.forEach( function (petition) {
                    MyApp.MyPetCreated.petitions[i] = {
                        "key":petition.key,
                        "name" : petition.properties.name,
                        "owner":petition.properties.owner,
                        "body":petition.properties.body,
                        "tags":petition.properties.tags.join(),
                        "nbVotants":petition.properties.nbVotants,
                    };
                    i++;
                });
                if ('nextPageToken' in response) {
                    MyApp.Profile.userData.nextToken = response.nextPageToken;
                } else {
                    MyApp.Profile.nextToken="";
                }
            }
        });
    },
    

    deletePetition: function (votePet) {
	    var data = {
            'id': votePet,
            'access_token' : encodeURIComponent(MyApp.Profile.userData.id)
        };
        m.request ({
	 		method: "POST",
            url: "_ah/api/myApi/v1/petition/delete",
            params: data,
		}).then(function(response){
            MyApp.MyPetCreated.getMyPetCreated();
            m.redraw();
        });
    },
    

}