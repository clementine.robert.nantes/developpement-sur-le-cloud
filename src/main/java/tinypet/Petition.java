package tinypet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;


public class Petition {
	
	public String id;
	public String name;
	public String body;
	public ArrayList<String> tags;
	public ArrayList<String> votants;
	public String owner;
	public Integer nbVotes;
	
	//Création des getteurs et des setteurs
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
	public Integer getNbVotes() {
		return nbVotes;
	}
	public void setNbVotes(Integer nbVotes) {
		this.nbVotes = nbVotes;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
