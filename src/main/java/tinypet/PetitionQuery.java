package tinypet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.KeyRange;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.repackaged.com.google.datastore.v1.CompositeFilter;
import com.google.appengine.repackaged.com.google.datastore.v1.Projection;
import com.google.appengine.repackaged.com.google.datastore.v1.PropertyFilter;

@WebServlet(name = "PetQuery", urlPatterns = { "/pquery" })
public class PetitionQuery extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Query q;
		PreparedQuery pq;
		List<Entity> result;
		Entity last;
		
		response.getWriter().print("<h2> Les pétitions signées par l'utilisateur U958</h2>");
		
		q = new Query("Petition").setFilter(new FilterPredicate("votants", FilterOperator.EQUAL, "U958"));

		pq = datastore.prepare(q);
		result = pq.asList(FetchOptions.Builder.withLimit(10));

		response.getWriter().print("<li> result:" + result.size() + "<br>");
		last=null;
		for (Entity entity : result) {
			response.getWriter().print("<li> Vous avez signé " + entity.getKey());
			last=entity;
		}
		
		response.getWriter().print("<h2> Les 10 pétitions les plus signées</h2>");
		
		q = new Query("Petition").addSort("nbVotants", SortDirection.DESCENDING);

		pq = datastore.prepare(q);
		result = pq.asList(FetchOptions.Builder.withLimit(10));

		response.getWriter().print("<li> result:" + result.size() + "<br>");
		last=null;
		int i = 0;
		for (Entity entity : result) {
			i++;
			response.getWriter().print("<li>" +i+". " + entity.getKey() + " avec " + entity.getProperty("nbVotants") + " votants");
			last=entity;
		}
		
		
		
		
		response.getWriter().print("<h2> finall 5 PU where key > P0 </h2>");

		
		Key k = KeyFactory.createKey("PU", "P0");

		q = new Query("PU").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.GREATER_THAN, k));

		 pq = datastore.prepare(q);
		result = pq.asList(FetchOptions.Builder.withLimit(5));

		response.getWriter().print("<li> result:" + result.size() + "<br>");
		last=null;
		for (Entity entity : result) {
			response.getWriter().print("<li>" + entity.getKey());
			last=entity;
		}

		response.getWriter().print("<h2> Great, get the next 10 results now </h2>");

		
		// One way to paginate...
		/*
		 * q = new Query("PU").setFilter(new FilterPredicate("__key__",
		 * FilterOperator.GREATER_THAN, last.getKey()));
		 * 
		 * pq = datastore.prepare(q); result =
		 * pq.asList(FetchOptions.Builder.withLimit(10));
		 * 
		 * response.getWriter().print("<li> result:" + result.size() + "<br>");
		 * last=null; for (Entity entity : result) { response.getWriter().print("<li>" +
		 * entity.getKey()); last=entity; }
		 */
		
		
		
		
		
		

		
	}
}
