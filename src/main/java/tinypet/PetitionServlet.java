package tinypet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.KeyRange;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import java.text.ParseException;
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.sql.Timestamp;

@WebServlet(name = "PetServlet", urlPatterns = { "/petition" })
public class PetitionServlet extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		

		Random r = new Random();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		

		// Create users
		for (int i = 0; i < 500; i++) {
			
				Date date = new Date();
				String sDate = getDate();
				
				try {
					date = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			
			
				Entity e = new Entity("Petition", new Timestamp(date.getTime()) + "P" + i );
				e.setProperty("Date", date);
				int owner = r.nextInt(1000); // Un user entre 0 et 1000 user
				e.setProperty("owner", "U" + owner+"@gmail.com"); 

				e.setProperty("name", "Petition "+owner);
	
				
				e.setProperty("body", "blabla");
				
				// Creation random des votants
				HashSet<String> fset = new HashSet<String>();
				for (int j=0; j<200; j++) {
					fset.add("U" + r.nextInt(1000));
				}
				
				e.setProperty("votants", fset);
				
				// Création d'un compteur pour le nombre de votants
				e.setProperty("nbVotants", fset.size());
				 
				// Creation random des tags
				HashSet<String> ftags = new HashSet<String>();
				while (ftags.size() < 10) {
					ftags.add("T" + r.nextInt(200));
				}
				
				e.setProperty("tags", ftags);
				
				 
				
				
				datastore.put(e);
				response.getWriter().print("<li> created post:" + e.getKey() + "<br>");
			
		}
		
		
		
	}
	
	public String getDate() {
		int day = 0, month = 0, year = 0;
		
		day = 1 + (int)(Math.random() * ((30 - 1) + 1));
		
		month = 1 + (int)(Math.random() * ((12 - 1) + 1));
		
		year = 2019 + (int)(Math.random() * ((2021 - 2019) + 1));
			
		return day+"/"+month+"/"+year;
	}
}