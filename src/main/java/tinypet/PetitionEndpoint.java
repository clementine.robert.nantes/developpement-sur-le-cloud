package tinypet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import java.util.HashSet;

import java.util.Arrays;

import com.google.api.server.spi.auth.common.User;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.EntityNotFoundException;

import java.sql.Timestamp;

@Api(name = "myApi",
     version = "v1",
     audiences = "593310797272-uo5kpvrgtk88p33h8jrd6bdeq1714h2v.apps.googleusercontent.com",
  	 clientIds = "593310797272-uo5kpvrgtk88p33h8jrd6bdeq1714h2v.apps.googleusercontent.com",
     namespace =
     @ApiNamespace(
		   ownerDomain = "helloworld.example.com",
		   ownerName = "helloworld.example.com",
		   packagePath = "")
     )

public class PetitionEndpoint {
	
	
	@ApiMethod(name = "myPetCreated", path = "petition/myPetCreated", httpMethod = ApiMethod.HttpMethod.GET)
	public CollectionResponse<Entity> myPetCreated(User user, 
			 @Named("email") String email, @Nullable @Named("next") String cursorString) throws Exception{
		
        if (user == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		Query q = new Query("Petition").setFilter(new FilterPredicate("owner", FilterOperator.EQUAL, email));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq = datastore.prepare(q);

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(10);

		if (cursorString != null) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(cursorString));
		}

		QueryResultList<Entity> results = pq.asQueryResultList(fetchOptions);
		cursorString = results.getCursor().toWebSafeString();

		return CollectionResponse.<Entity>builder().setItems(results).setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "myPetSigned", path = "petition/myPetSigned", httpMethod = ApiMethod.HttpMethod.GET)
	public CollectionResponse<Entity> myPetSigned(User user, 
			 @Named("email") String email, @Nullable @Named("next") String cursorString) throws Exception{
		
        if (user == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		Query q = new Query("Petition").setFilter(new FilterPredicate("votants", FilterOperator.EQUAL, email));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq = datastore.prepare(q);

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(10);

		if (cursorString != null) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(cursorString));
		}

		QueryResultList<Entity> results = pq.asQueryResultList(fetchOptions);
		cursorString = results.getCursor().toWebSafeString();

		return CollectionResponse.<Entity>builder().setItems(results).setNextPageToken(cursorString).build();
	}

	@ApiMethod(name = "topPet", path = "petition/topPet", httpMethod = ApiMethod.HttpMethod.GET)
	public CollectionResponse<Entity> topPet(User user, @Nullable @Named("next") String cursorString) throws Exception{
		
        if (user == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		Query q = new Query("Petition").addSort("nbVotants", SortDirection.DESCENDING);

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq = datastore.prepare(q);

		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(10);

		if (cursorString != null) {
			fetchOptions.startCursor(Cursor.fromWebSafeString(cursorString));
		}

		QueryResultList<Entity> results = pq.asQueryResultList(fetchOptions);
		cursorString = results.getCursor().toWebSafeString();

		return CollectionResponse.<Entity>builder().setItems(results).setNextPageToken(cursorString).build();
	}


	@ApiMethod(name = "deletePetition", path = "petition/delete", httpMethod = HttpMethod.POST)
	public Entity deletePetition(User user, Petition petition) throws UnauthorizedException {

		if (user == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Transaction deletePetTransaction = datastore.beginTransaction();
		
		Key petitionKey = KeyFactory.createKey("Petition", petition.getId());

		datastore.delete(petitionKey);
		deletePetTransaction.commit();

		return null;
	}
	

	@ApiMethod(name = "postPet",  path="petition/postPet", httpMethod = HttpMethod.POST)
	public Entity postPet(User user, Petition petition) throws UnauthorizedException {

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        //verify the credentials
        if(user == null) { 
            throw new UnauthorizedException("Invalid credentials");
        }


		//Ne fonctionne pas 
		/*HashSet<String> ftags = new HashSet<String>();
		String[] tagsArray = petition.tags.toString().split(",");


		for (int i=0; i<tagsArray.length; i++) 
		{ 
			ftags.add(tagsArray[i]);
		}*/
		

		//Foreach et insérer dans le hashset
		
		//Si l'utilisateur crée une pétition on peut en déduire qu'il vote pour cette pétition, cela permet également de ne pas avoir un tableau vide et donc éviter des erreurs.
		//votant.add(user.getEmail());

		Date todayDate = new Date();
		// Pour pas que les tableaux soient null
		ArrayList <String> votants = new ArrayList();
		//ArrayList <String> tagsArray = new ArrayList();
		
		votants.add("");
		
		//tagsArray.add("");
        Entity e = new Entity("Petition", new Timestamp(todayDate.getTime())+":"+user.getEmail());
        e.setProperty("owner", user.getEmail());
        e.setProperty("name", petition.name);
        e.setProperty("body", petition.body);
        e.setProperty("tags", petition.tags);
        e.setProperty("votants", votants);
        e.setProperty("nbVotants", 0);
        e.setProperty("petitionDate", new Date());

        Transaction transaction = datastore.beginTransaction();
        datastore.put(e);
        transaction.commit();
        return e;
	
		
	}

	@ApiMethod(name = "newVote", path="petition/newVote",httpMethod = HttpMethod.POST)
	public Entity newVote(User user, Vote vote) throws UnauthorizedException {

		if (user.getEmail() == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Query queryVote = new Query("Vote")
				.setFilter(CompositeFilterOperator.and(
					new FilterPredicate("Email", FilterOperator.EQUAL, user.getEmail()),
					new FilterPredicate("Petition", FilterOperator.EQUAL, vote.getPetitionChoosed())
					)
				);

		PreparedQuery preparedVoteQuery = datastore.prepare(queryVote.setKeysOnly());

		Entity voteQueryResult = preparedVoteQuery.asSingleEntity();

		if (voteQueryResult == null) {
			Entity e = new Entity("Vote");
			e.setProperty("Email", user.getEmail());
			e.setProperty("Petition", vote.getPetitionChoosed());

			Transaction voteInsertTransaction = datastore.beginTransaction();
			datastore.put(e);
			voteInsertTransaction.commit();
			Entity petition;
			try {
				Transaction petitionAddVoteTransaction = datastore.beginTransaction();
				petition = datastore.get(KeyFactory.createKey("Petition", vote.getPetitionChoosed()));
				// Ajoute 1 au nombre de votants
				petition.setProperty("nbVotants", (Long) petition.getProperty("nbVotants")+1);

				// Ajoute l'utilisateur au tableau des votants
				@SuppressWarnings("unchecked")
				ArrayList<String> votants = (ArrayList<String>) petition.getProperty("votants");
				votants.add(user.getEmail());
				petition.setProperty("votants", votants);

				datastore.put(petition);
				petitionAddVoteTransaction.commit();
				return petition;
			} catch (EntityNotFoundException e1) {
				// If the entity is not found we just log the error
				e1.printStackTrace();
				return e;
			}
		} else {
			datastore.delete(voteQueryResult.getKey());
			Entity petition;
			try {
				Transaction removeVotePetitionTransaction = datastore.beginTransaction();
				petition = datastore.get(KeyFactory.createKey("Petition", vote.getPetitionChoosed()));
				
				// Supprimer 1 au nombre de votes
				if((Long)petition.getProperty("nbVotants")!=0){
					petition.setProperty("nbVotants", (Long)petition.getProperty("nbVotants")-1);
				} 
				
				// Supprime l'utilisateur au tableau des votants
				@SuppressWarnings("unchecked")
				ArrayList<String> votants = (ArrayList<String>) petition.getProperty("votants");
				votants.remove(user.getEmail());
				petition.setProperty("votants", votants);
				
				datastore.put(petition);
				removeVotePetitionTransaction.commit();
				return petition;

			} catch (EntityNotFoundException e1) {
				// If the entity is not found we just log the error
				e1.printStackTrace();
				return null;
			}
		}
    }

	@ApiMethod(name= "createUser", path="user/createUser", httpMethod = HttpMethod.POST)
	public Entity createUser(User user, UserPet userPet) throws UnauthorizedException {

		if (user == null) {
			throw new UnauthorizedException("Invalid credentials");
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	    Query searchQuery = new Query("User").setFilter(
	    		new FilterPredicate("email", FilterOperator.EQUAL, userPet.email));

	    PreparedQuery preparedSearchQuery = datastore.prepare(searchQuery);

		Entity searchQueryResult = preparedSearchQuery.asSingleEntity();

		if(searchQueryResult == null) {
			Entity newUserPet = new Entity("User");
			newUserPet.setProperty("email", userPet.email);
			newUserPet.setProperty("name", userPet.name);
			newUserPet.setProperty("invertedName", userPet.invertedName);
			newUserPet.setProperty("firstName", userPet.firstName);
			newUserPet.setProperty("lastName", userPet.lastName);
			newUserPet.setProperty("url", userPet.url);

			Transaction newUserPetTransaction = datastore.beginTransaction();
			datastore.put(newUserPet);
			newUserPetTransaction.commit();
			return newUserPet;
		}
		return searchQueryResult;
	}

	@ApiMethod(name = "searchPetition", path="petition/search", httpMethod = ApiMethod.HttpMethod.GET)
	public CollectionResponse<Entity> searchPetition(User user, @Named("recherche") String recherche, @Nullable @Named("next") String cursorString) throws Exception{

        if (user == null) {
            throw new UnauthorizedException("Invalid credentials");
        }

		Query q = new Query("Petition").setFilter(
			new CompositeFilter(CompositeFilterOperator.OR, Arrays.asList(
				new FilterPredicate("name", FilterOperator.EQUAL, recherche),
				new FilterPredicate("tags", FilterOperator.EQUAL, recherche)
		)));

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        PreparedQuery preparedSearchQuery = datastore.prepare(q);
        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(10);

		if (cursorString != null) {
            fetchOptions.startCursor(Cursor.fromWebSafeString(cursorString));
            QueryResultList<Entity> results = preparedSearchQuery.asQueryResultList(fetchOptions);
            cursorString = results.getCursor().toWebSafeString();

            return CollectionResponse.<Entity>builder().setItems(results).setNextPageToken(cursorString).build();
		} else {
            return CollectionResponse.<Entity>builder().setItems(preparedSearchQuery.asQueryResultList(fetchOptions)).build();
        }
	}
	
	
	
	
	
}
